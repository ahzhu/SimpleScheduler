# MiniScheduler

#### 介绍
简单的任务调度程序，定时任务(支持Cron表达式)，延时队列，不支持分布式，延时队列支持失败重试，支持设置失败次数

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明


        static void Main(string[] args)
        {
            IDelayQueue delayQueue = MiniSchedulerFactory.CreateDelayQueue(new QueueOptions() { EnableRetry = true });
            delayQueue.Enqueue(new QueueItem()
            {
                TaskName = "Test01",
                DelayTime = TimeSpan.FromSeconds(10),
                DelayAction = () =>
                {
                    Console.WriteLine("Test01 Action {0}", DateTime.Now.ToString());
                    return true;
                }
            });
            delayQueue.Enqueue(new QueueItem<string>()
            {
                TaskName = "Test02",
                DelayTime = TimeSpan.FromSeconds(16),
                ActionData = "TestData",
                EnableRetry = true,
                DelayAction = (str) =>
                {
                    Console.WriteLine("Test02 Action： {0}, Data： {1}", DateTime.Now.ToString(), str);
                    return false;
                }
            });

            //定时任务
            IScheduleTask schedule = MiniSchedulerFactory.CreateSchedule();
            schedule.AddTask(new ScheduleTaskItem("10-30/5 * * * * ?", () =>
            {
                Console.WriteLine("Schedule Task 1: {0}", DateTime.Now.ToString());
                return true;
            }));

            List<int> list = new List<int>(){ 1 };
            schedule.AddTask(new ScheduleTaskItem<List<int>>("5 * * * * ?", list, (data) =>
            {
                Console.WriteLine("Schedule Task 2: {0}", DateTime.Now.ToString());
                return true;
            }));


            Console.ReadLine();
        }

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
