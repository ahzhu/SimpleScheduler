﻿using CronParse;

namespace SimpleScheduler
{
    /// <summary>
    /// 延时队列带参数
    /// </summary>
    public sealed class QueueItem<T> : TaskItem
    {
        public QueueItem() { }

        public QueueItem(T item, Func<T, bool>? callback) 
        {
            ActionData = item;
            DelayAction = callback;
        }

        public QueueItem(TimeSpan delayTime, T item, Func<T, bool>? callback)
        {
            DelayTime = delayTime;
            ActionData = item;
            DelayAction = callback;
        }

        /// <summary>
        /// 延时回调数据
        /// </summary>
        public T? ActionData { get; set; }

        /// <summary>
        /// 延时回调
        /// </summary>
        public Func<T, bool>? DelayAction { get; set; }

        public override bool? ExecuteAction()
        {
            return DelayAction?.Invoke(ActionData);
        }
    }

    /// <summary>
    /// 延时队列不支持参数
    /// </summary>
    public sealed class QueueItem : TaskItem
    {
        public QueueItem() { }

        public QueueItem(Func<bool>? callback) 
        {
            DelayAction = callback;
        }

        public QueueItem(TimeSpan delayTime, Func<bool>? callback)
        {
            DelayTime = delayTime;
            DelayAction = callback;
        }

        /// <summary>
        /// 延时回调
        /// </summary>
        public Func<bool>? DelayAction { get; set; }

        public override bool? ExecuteAction()
        {
            return DelayAction?.Invoke();
        }
    }

}
