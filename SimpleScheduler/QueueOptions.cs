﻿namespace SimpleScheduler
{
    /// <summary>
    /// 延时队列参数
    /// </summary>
    public sealed class QueueOptions
    {
        /// <summary>
        /// 共享查询，用于查询所有队列，是否存在延时任务
        /// </summary>
        public bool ShareExists { get; set; }

        /// <summary>
        /// 启用重试
        /// </summary>
        public bool EnableRetry { get; set; }

        /// <summary>
        /// 重试次数，默认为 1, 最大10, <see cref="EnableRetry"/>为 True 时生效。
        /// </summary>
        public ushort RetryMax { get; set; } = 1;

        /// <summary>
        /// 队列延时精度
        /// </summary>
        public QueueDelayAccuracy DelayAccuracy { get; set; }

        /// <summary>
        /// 队列处理时的并发数，默认为 1
        /// </summary>
        public int QueueHandledConcurrency { get; set; } = 1;
    }

    /// <summary>
    /// 队列延时精度
    /// </summary>
    public enum QueueDelayAccuracy
    {
        /// <summary>
        /// 秒级
        /// </summary>
        Second = 0,

        /// <summary>
        /// 分钟
        /// </summary>
        Minute = 1,

        /// <summary>
        /// 毫秒级
        /// </summary>
        Milliseconds = 2,
    }
}
