﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleScheduler
{
    /// <summary>
    /// 定时任务
    /// </summary>
    public interface IScheduleTask : IDelayBase
    {
        /// <summary>
        /// 添加定时任务入队，回调时不支持参数
        /// </summary>
        /// <param name="taskItem">定时任务</param>
        /// <returns></returns>
        public ValueTask<bool> Enqueue(ScheduleTaskItem taskItem);

        /// <summary>
        /// 添加定时任务入队，回调时支持参数
        /// </summary>
        /// <typeparam name="T">定时任务参数</typeparam>
        /// <param name="taskItem">定时任务</param>
        /// <returns></returns>
        public ValueTask<bool> Enqueue<T>(ScheduleTaskItem<T> taskItem);

        /// <summary>
        /// 定时任务是否存在
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Exist(ScheduleTaskItem item);

        /// <summary>
        /// 定时任务是否存在
        /// </summary>
        /// <typeparam name="T">定时任务参数</typeparam>
        /// <param name="item">定时任务</param>
        /// <returns></returns>
        public bool Exist<T>(ScheduleTaskItem<T> item);
    }
}
