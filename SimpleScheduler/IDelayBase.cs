﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleScheduler
{
    public interface IDelayBase
    {
        /// <summary>
        /// 启动队列，队列创建时默认是启动的
        /// </summary>
        public void Start();

        /// <summary>
        /// 停止队列
        /// </summary>
        public void Stop();

        /// <summary>
        /// 清空队列
        /// </summary>
        public void Clear();
    }
}
