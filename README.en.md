# MiniScheduler

#### Description
A simple scheduler, timed tasks (supports Cron expressions), delayed queues, does not support distributed, delayed queues support failure retry, support for setting the number of failures

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

        static void Main(string[] args)
        {
            IDelayQueue delayQueue = MiniSchedulerFactory.CreateDelayQueue(new QueueOptions() { EnableRetry = true });
            delayQueue.Enqueue(new QueueItem()
            {
                TaskName = "Test01",
                DelayTime = TimeSpan.FromSeconds(10),
                DelayAction = () =>
                {
                    Console.WriteLine("Test01 Action {0}", DateTime.Now.ToString());
                    return true;
                }
            });
            delayQueue.Enqueue(new QueueItem<string>()
            {
                TaskName = "Test02",
                DelayTime = TimeSpan.FromSeconds(16),
                ActionData = "TestData",
                EnableRetry = true,
                DelayAction = (str) =>
                {
                    Console.WriteLine("Test02 Action： {0}, Data： {1}", DateTime.Now.ToString(), str);
                    return false;
                }
            });

            //Schedule Task
            IScheduleTask schedule = MiniSchedulerFactory.CreateSchedule();
            schedule.AddTask(new ScheduleTaskItem("10-30/5 * * * * ?", () =>
            {
                Console.WriteLine("Schedule Task 1: {0}", DateTime.Now.ToString());
                return true;
            }));

            List<int> list = new List<int>(){ 1 };
            schedule.AddTask(new ScheduleTaskItem<List<int>>("5 * * * * ?", list, (data) =>
            {
                Console.WriteLine("Schedule Task 2: {0}", DateTime.Now.ToString());
                return true;
            }));


            Console.ReadLine();
        }

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
