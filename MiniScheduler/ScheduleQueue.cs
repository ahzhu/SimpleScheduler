﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace MiniScheduler
{
    internal class ScheduleQueue : IScheduleTask
    {
        private readonly PeriodicTimer _checkTimer;
        private readonly PeriodicTimer _queueTimer;
        private readonly ConcurrentDictionary<DelayQueueKey, IList<TaskItem>> _queueItems;
        private readonly SortedDictionary<ulong, Queue<DelayQueueKey>> _queueSortValues;
        private readonly ActionBlock<DelayQueueKey> _plusTimerCount;
        private readonly BufferBlock<Queue<DelayQueueKey>> _queueBuffer;
        private readonly ActionBlock<Queue<DelayQueueKey>> _queueAction;
        private readonly ActionBlock<ulong> _timeFinishAction;
        private readonly ActionBlock<(DelayQueueKey key, TaskItem queueItem)> _nextAction;
        private readonly string _queueName;
        private readonly TimeSpan _delayAccuracy;
        private readonly bool _shareQuery = false;
        private readonly Func<DelayQueueKey, bool> _shareExistFunc;

        private CancellationTokenSource CancelToken { get; set; }

        private QueueOptions _queueOptions;
        /// <summary>
        /// 暂停计时
        /// </summary>
        volatile bool PauseCount;
        volatile bool IsStarted;
        ulong MinQueueKey;
        ulong _timerCount = ulong.MinValue;

        internal ScheduleQueue(string queueName, QueueOptions opts, Func<DelayQueueKey, bool> existFunc) : this(queueName, opts)
        {
            _shareExistFunc = existFunc;
        }

        internal ScheduleQueue(string queueName, QueueOptions opts)
        {
            _queueName = queueName ?? Constants.DefaultQueueName;
            _queueOptions = opts;
            _shareQuery = opts?.ShareExists == true;
            _delayAccuracy = TimeSpan.FromSeconds(1);
            int max = opts?.QueueHandledConcurrency ?? 1;
            _queueBuffer = new BufferBlock<Queue<DelayQueueKey>>();
            _queueAction = new ActionBlock<Queue<DelayQueueKey>>(QueueBlockAction, new ExecutionDataflowBlockOptions() { MaxDegreeOfParallelism = max });
            _queueBuffer.LinkTo(_queueAction);
            _queueItems = new ConcurrentDictionary<DelayQueueKey, IList<TaskItem>>();
            _queueSortValues = new SortedDictionary<ulong, Queue<DelayQueueKey>>();
            _plusTimerCount = new ActionBlock<DelayQueueKey>(SetTimerCount, new ExecutionDataflowBlockOptions() { MaxDegreeOfParallelism = 1 });
            _timeFinishAction = new ActionBlock<ulong>(TimeFinished, new ExecutionDataflowBlockOptions() { MaxDegreeOfParallelism = 1 });
            _nextAction = new ActionBlock<(DelayQueueKey key, TaskItem queueItem)>(AddNext, new ExecutionDataflowBlockOptions() { MaxDegreeOfParallelism = 1 });
            CancelToken = new CancellationTokenSource();

            _checkTimer = new PeriodicTimer(TimeSpan.FromMinutes(10));
            _queueTimer = new PeriodicTimer(_delayAccuracy);
            StartCheck();
        }

        async Task StartCheck()
        {
            while (await _checkTimer.WaitForNextTickAsync())
            {
                if (_queueSortValues.Count < 1)
                {
                    _timerCount = ulong.MinValue;
                }
            }
        }

        async Task StartTimer()
        {
            if (IsStarted)
            {
                return;
            }
            CancelToken = new CancellationTokenSource();
            IsStarted = true;
            while (await _queueTimer.WaitForNextTickAsync(CancelToken.Token))
            {
                if (PauseCount)
                {
                    continue;
                }
                _timerCount++;
                if (MinQueueKey > ulong.MinValue && _timerCount == MinQueueKey)
                {
                    await _timeFinishAction.SendAsync(MinQueueKey);
                }
                MinQueueKey = _queueSortValues.Count < 1 ? ulong.MinValue : _queueSortValues.Keys.ElementAt(0);
            }
            IsStarted = false;
        }

        void TimeFinished(ulong arg)
        {
            if (arg == ulong.MinValue)
            {
                return;
            }
            if (_queueSortValues.TryGetValue(arg, out Queue<DelayQueueKey> value))
            {
                _queueBuffer.Post(value);
                _queueSortValues.Remove(arg);
            }
        }

        void SetTimerCount(DelayQueueKey arg)
        {
            PauseCount = true;
            try
            {
                double delayVal = 0;
                if (arg.IsScheduleTask)
                {
                    DateTime now = DateTime.Now;
                    var cron = arg.Cron.Value;
                    DateTime nextTime = cron.GetNextTime(now);
                    var timesp = nextTime - now;
                    delayVal = timesp / _delayAccuracy;
                }
                else
                {
                    delayVal = arg.DelayTime / _delayAccuracy;
                }
                ulong minCount = delayVal < 1 ? 1 : (ulong)Math.Ceiling(delayVal);
                Queue<DelayQueueKey> queue;
                if (_queueSortValues.Count < 1)
                {
                    queue = new Queue<DelayQueueKey>();
                    queue.Enqueue(arg);
                    _queueSortValues.TryAdd(minCount, queue);
                    //重置计时标记
                    _timerCount = ulong.MinValue;
                }
                else
                {
                    minCount = _timerCount + minCount;
                    if (_queueSortValues.TryGetValue(minCount, out queue))
                    {
                        queue.Enqueue(arg);
                    }
                    else
                    {
                        queue = new Queue<DelayQueueKey>();
                        queue.Enqueue(arg);
                        _queueSortValues.TryAdd(minCount, queue);
                    }
                }
                MinQueueKey = _queueSortValues.Keys.ElementAt(0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                PauseCount = false;
            }
        }

        TimeSpan GetDelayAccuracy(QueueDelayAccuracy? level)
        {
            return level switch
            {
                QueueDelayAccuracy.Minute => TimeSpan.FromMinutes(1),
                QueueDelayAccuracy.Milliseconds => TimeSpan.FromMilliseconds(1),
                _ => TimeSpan.FromSeconds(1),
            };
        }

        async Task QueueBlockAction(Queue<DelayQueueKey> arg)
        {
            while (arg?.Count > 0)
            {
                if (!arg.TryDequeue(out DelayQueueKey key))
                {
                    return;
                }
                if (_queueItems.TryRemove(key, out IList<TaskItem> taskQueue))
                {
                    Parallel.ForEach(taskQueue, (tq) =>
                    {
                        if (tq == null || tq.IsCancellation)
                        {
                            return;
                        }
                        bool? result = tq.ExecuteAction();
                        _nextAction.SendAsync(ValueTuple.Create(key, tq));
                    });
                }
            }
        }

        async Task AddNext((DelayQueueKey key, TaskItem taskItem) item)
        {
            if (!item.key.IsScheduleTask)
            {
                return;
            }
            await _plusTimerCount.SendAsync(item.key);
            IList<TaskItem> queueModel;
            if (_queueItems.TryGetValue(item.key, out queueModel))
            {
                queueModel.Add(item.taskItem);
            }
            else
            {
                queueModel = new List<TaskItem>() { item.taskItem };
                _queueItems.TryAdd(item.key, queueModel);
            }
        }

        internal bool Exist(DelayQueueKey key)
        {
            return _queueItems.ContainsKey(key);
        }

        public void Start()
        {
            StartTimer();
            Console.WriteLine("Started: {0}", DateTime.Now);
        }

        public void Stop()
        {
            if (!IsStarted)
            {
                return;
            }
            CancelToken?.Cancel(false);
            IsStarted = false;
        }

        public async void Clear()
        {
            _queueItems.Clear();
            while (PauseCount)
            {
                await Task.Delay(100);
            }
            _queueSortValues.Clear();
        }

        public async ValueTask<bool> AddTask(ScheduleTaskItem taskItem)
        {
            if (taskItem == null)
            {
                return false;
            }
            taskItem.QueueName = string.IsNullOrWhiteSpace(taskItem.QueueName) ? _queueName : taskItem.QueueName;
            if (!string.Equals(_queueName, taskItem.QueueName))
            {
                return false;
            }
            var taskKey = new DelayQueueKey
            {
                QueueName = taskItem.QueueName,
                TaskName = taskItem.TaskName,
                CronExpression = taskItem.CronExpression,
                IsScheduleTask = taskItem.IsScheduleTask,
                Cron = new CronParse.CronStruct(taskItem.CronExpression),
            };
            await _plusTimerCount.SendAsync(taskKey);
            IList<TaskItem> queueModel;
            if (_queueItems.TryGetValue(taskKey, out queueModel))
            {
                queueModel.Add(taskItem);
                return true;
            }
            else
            {
                queueModel = new List<TaskItem>() { taskItem };
                return _queueItems.TryAdd(taskKey, queueModel);
            }
        }

        public async ValueTask<bool> AddTask<T>(ScheduleTaskItem<T> taskItem)
        {
            if (taskItem == null)
            {
                return false;
            }
            taskItem.QueueName = string.IsNullOrWhiteSpace(taskItem.QueueName) ? _queueName : taskItem.QueueName;
            if (!string.Equals(_queueName, taskItem.QueueName))
            {
                return false;
            }
            var taskKey = new DelayQueueKey
            {
                QueueName = taskItem.QueueName,
                TaskName = taskItem.TaskName,
                CronExpression = taskItem.CronExpression,
                IsScheduleTask = taskItem.IsScheduleTask,
                Cron = new CronParse.CronStruct(taskItem.CronExpression),
            };
            await _plusTimerCount.SendAsync(taskKey);
            IList<TaskItem> queueModel;
            if (_queueItems.TryGetValue(taskKey, out queueModel))
            {
                queueModel.Add(taskItem);
                return true;
            }
            else
            {
                queueModel = new List<TaskItem>() { taskItem };
                return _queueItems.TryAdd(taskKey, queueModel);
            }
        }

        public bool Exist(ScheduleTaskItem item)
        {
            var key = new DelayQueueKey()
            {
                QueueName = item.QueueName,
                TaskName = item.TaskName,
                CronExpression = item.CronExpression,
                IsScheduleTask = item.IsScheduleTask,
            };
            if (_queueItems.ContainsKey(key))
            {
                return true;
            }
            if (_shareQuery && _shareExistFunc != null)
            {
                return _shareExistFunc.Invoke(key);
            }
            return false;
        }

        public bool Exist<T>(ScheduleTaskItem<T> item)
        {
            var key = new DelayQueueKey()
            {
                QueueName = item.QueueName,
                TaskName = item.TaskName,
                CronExpression = item.CronExpression,
                IsScheduleTask = item.IsScheduleTask,
            };
            if (_queueItems.ContainsKey(key))
            {
                return true;
            }
            if (_shareQuery && _shareExistFunc != null)
            {
                return _shareExistFunc.Invoke(key);
            }
            return false;
        }

    }
}
