﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CronParse;

namespace MiniScheduler
{
    /// <summary>
    /// 定时任务不支持参数
    /// </summary>
    public sealed class ScheduleTaskItem : TaskItem
    {
        public ScheduleTaskItem()
        {
            QueueName = Constants.DefalutScheduleName;
        }

        /// <summary>
        /// 定时任务，默认每分钟执行一次
        /// </summary>
        /// <param name="callback"></param>
        public ScheduleTaskItem(Func<bool>? callback) : this("0 */1 * * * ?", callback)
        {
        }

        /// <summary>
        /// 定时任务
        /// </summary>
        /// <param name="cronExpr">表达式</param>
        /// <param name="callback">任务回调</param>
        public ScheduleTaskItem(string cronExpr, Func<bool>? callback)
        {
            QueueName = Constants.DefalutScheduleName;
            IsScheduleTask = true;
            _CronExpression = cronExpr;
            ScheduleAction = callback;
        }

        private string _CronExpression = string.Empty;
        /// <summary>
        /// Cron表达式
        /// </summary>
        public string CronExpression
        {
            get => _CronExpression;
            set
            {
                _CronExpression = value;
                IsScheduleTask = true;
            }
        }

        /// <summary>
        /// 定时回调
        /// </summary>
        public Func<bool>? ScheduleAction { get; set; }

        public override bool? ExecuteAction()
        {
            return ScheduleAction?.Invoke();
        }
    }

    /// <summary>
    /// 定时任务不支持参数
    /// </summary>
    /// <typeparam name="T">参数类型</typeparam>
    public sealed class ScheduleTaskItem<T> : TaskItem
    {
        public ScheduleTaskItem()
        {
            QueueName = Constants.DefalutScheduleName;
        }

        /// <summary>
        /// 定时任务，默认每分钟执行一次
        /// </summary>
        /// <param name="callback"></param>
        public ScheduleTaskItem(T item, Func<T, bool>? callback) : this("0 */1 * * * ?", item, callback)
        {
        }

        /// <summary>
        /// 定时任务
        /// </summary>
        /// <param name="cronExpr">表达式</param>
        /// <param name="callback">任务回调</param>
        public ScheduleTaskItem(string cronExpr, T item, Func<T, bool>? callback)
        {
            QueueName = Constants.DefalutScheduleName;
            IsScheduleTask = true;
            _CronExpression = cronExpr;
            ScheduleData = item;
            ScheduleAction = callback;
        }

        private string _CronExpression = string.Empty;
        /// <summary>
        /// Cron表达式
        /// </summary>
        public string CronExpression
        {
            get => _CronExpression;
            set
            {
                _CronExpression = value;
                IsScheduleTask = true;
            }
        }

        /// <summary>
        /// 延时回调数据
        /// </summary>
        public T? ScheduleData { get; set; }

        /// <summary>
        /// 延时回调
        /// </summary>
        public Func<T, bool>? ScheduleAction { get; set; }

        public override bool? ExecuteAction()
        {
            return ScheduleAction?.Invoke(ScheduleData);
        }
    }

}
