﻿namespace MiniScheduler
{
#if NET6_0_OR_GREATER

    /// <summary>
    /// 延时队列
    /// </summary>
    public interface IDelayQueue : IDelayBase
    {
        /// <summary>
        /// 添加任务入队，回调时不支持参数
        /// </summary>
        /// <param name="callback">回调任务信息</param>
        /// <returns></returns>
        public ValueTask<bool> Enqueue(QueueItem callback);

        /// <summary>
        /// 添加任务入队，回调时支持参数
        /// </summary>
        /// <typeparam name="T">回调参数</typeparam>
        /// <param name="callback">回调任务信息</param>
        /// <returns></returns>
        public ValueTask<bool> Enqueue<T>(QueueItem<T> callback);

        /// <summary>
        /// 延时任务是否存在
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Exist(QueueItem item);

        /// <summary>
        /// 延时任务是否存在
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Exist<T>(QueueItem<T> item);

    }

#else

    /// <summary>
    /// 延时队列
    /// </summary>
    public interface IDelayQueue
    {
        /// <summary>
        /// 添加任务入队，回调时不支持参数
        /// </summary>
        /// <param name="callback">回调任务信息</param>
        /// <returns></returns>
        public Task<bool> Enqueue(QueueItem callback);

        /// <summary>
        /// 添加任务入队，回调时支持参数
        /// </summary>
        /// <typeparam name="T">回调参数</typeparam>
        /// <param name="callback">回调任务信息</param>
        /// <returns></returns>
        public Task<bool> Enqueue<T>(QueueItem<T> callback);

        /// <summary>
        /// 延时任务是否存在
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Exist(QueueItem item);

        /// <summary>
        /// 延时任务是否存在
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Exist<T>(QueueItem<T> item);

        /// <summary>
        /// 启动队列，队列创建时默认是启动的
        /// </summary>
        public void Start();

        /// <summary>
        /// 停止队列
        /// </summary>
        public void Stop();

        /// <summary>
        /// 清空队列
        /// </summary>
        public void Clear();
    }

#endif
}
